import argparse
import png
from pathlib import Path

parser = argparse.ArgumentParser(description='Convert png to C header')
parser.add_argument('file')
args = parser.parse_args()

cwd = Path('.')

with (cwd / 'src' / 'fx_image_data.h').open(mode='w') as header:
    with open(args.file, 'rb') as image:
        p = png.Reader(file=image)
        x, y, image, meta = p.read()
        palette = meta['palette']

        header.write(f'#include <framebuffer.h>\n')
        header.write(f'byte framebuffer[] = {{\n')
        for row in image:
            for pixel in row:
                header.write(f'{pixel},')
            header.write('\n')
        header.write(f'}};\n\n')

        header.write(f'CRGB clut[] = {{\n')
        for r, g, b in palette:
            header.write(f'{{ {r}, {g}, {b} }},\n')
        header.write(f'}};\n\n')

