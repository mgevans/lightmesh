import imageio
import numpy as np
import png
from pathlib import Path

cwd = Path('.')

with (cwd / 'src' / 'led_data.h').open(mode='w') as file:
    file.write(f'const PROGMEM LedWeight weight_table[] = {{')
    lengths = []
    for i, image_fn in enumerate(sorted((cwd / 'mesh').glob('mesh-led*.png'))):
        im = imageio.imread(f'{image_fn}')
        red = im[:,:,0]
        file.write(f'\n/* {image_fn} */\n')
        idx = np.flatnonzero(red > (0.4 * 0xff))
        lengths.append(len(idx))
        weights = np.clip(red.flat[idx] - (0.4 * 0xff), 0, 1 * 0xff).astype('int')
        v = np.zeros(red.shape)
        v.flat[idx] = weights.flat
        for pixel, weight in zip(idx, weights):
            file.write(f'{{ {pixel}, {weight} }},\n')
    file.write(f'}};\n\n')

    file.write(f'const PROGMEM LedWeightIndex weight_index[] = {{\n')
    offset = 0
    for length in lengths:
        file.write(f'{{ {offset}, {length} }},\n')
        offset = offset + length
    file.write(f'}};\n')

