#ifdef FX_IMAGE
#include <fx_image.h>
#define FX_INIT FX_IMAGE_init
#define FX_NEXT_FRAME FX_IMAGE_next_frame
#endif /* FX_IMAGE */

#if FX_FLAME
#include <fx_flame.h>
#define FX_INIT FX_FLAME_init
#define FX_NEXT_FRAME FX_FLAME_next_frame
#endif /* FX_FLAME */
