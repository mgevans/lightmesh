#include <FastLED.h>
#include <WProgram.h>

#define FRAMEBUFFER_WIDTH 160
#define FRAMEBUFFER_HEIGHT 120

extern byte framebuffer[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT];
extern CRGB clut[256];
