#include <WProgram.h>
#include <FastLED.h>
#include "framebuffer.h"

void FX_FLAME_init() {
  // Set bottom line to 37 (color white: 0xFF,0xFF,0xFF)
  for (auto i = 0; i < FRAMEBUFFER_WIDTH; i++) {
    framebuffer[(FRAMEBUFFER_HEIGHT-1)*FRAMEBUFFER_WIDTH + i] = 16;
  }
}

void spread_fire(size_t src) {
  auto pixel = framebuffer[src];
  if (pixel == 0) {
    framebuffer[src - FRAMEBUFFER_WIDTH] = 0;
  } else {
    auto randIdx = random(0, 3);
    auto dst = src - randIdx + 1;
    framebuffer[dst - FRAMEBUFFER_WIDTH] = pixel - (randIdx & 1);
  }
}

void FX_FLAME_next_frame() {
  for(auto x=0 ; x < FRAMEBUFFER_WIDTH; x++) {
    for (auto y = 1; y < FRAMEBUFFER_HEIGHT; y++) {
      spread_fire(y * FRAMEBUFFER_WIDTH + x);
    }
  }
}

