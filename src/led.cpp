#include <WProgram.h>
#include <FastLED.h>
#include "framebuffer.h"
#include "led.h"
#include "led_data.h"

CRGB leds[NUM_LEDS];

void LED_init() {
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(0xff);
  FastLED.setCorrection(LEDColorCorrection::Typical8mmPixel);
  FastLED.setMaxRefreshRate(30);
  //FastLED.setDither(0);
  fill_solid(leds, NUM_LEDS, CRGB::White);
}

void LED_copy() {
  // for each led
  for (int i = 0; i < NUM_LEDS; i++) {
    // get this led's section of the weight table
    LedWeightIndex lwi = weight_index[i];
    auto end = lwi.offset + lwi.len;

    CRGB led;
    // for each pixel weight
    for (unsigned long pw = lwi.offset; pw < end; pw++) {
      LedWeight lw = weight_table[pw];
      CRGB pixel = clut[framebuffer[lw.pixel]];
      led = blend(led, pixel, lw.weight);
    }

    leds[i] = led;
  }
}

