#include <FastLED.h>

#define NUM_LEDS 72
#define DATA_PIN 3

extern CRGB leds[NUM_LEDS];

struct LedWeightIndex {
  size_t offset;
  size_t len;
};

struct LedWeight {
  size_t pixel;
  byte weight;
};

void LED_init();
void LED_copy();
