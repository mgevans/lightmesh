#include <WProgram.h>
#include <FastLED.h>

#include "config.h"
#include "framebuffer.h"
#include "led.h"

extern "C" int main(void)
{
  setup();
  while (1) {
    loop();
    yield();
  }
}

void setup() {
  Serial.begin(115200);
  FX_INIT();
  LED_init();
  FastLED.show();
  delay(1000);
}

void loop() {
  FX_NEXT_FRAME();
  LED_copy();
  Serial.println();
  Serial.print("fps: ");
  Serial.println(FastLED.getFPS());
  FastLED.show();
}
