#!/bin/bash -xe

for i in mesh-path*.png; do convert $i -define histogram:unique-colors=true -format %c histogram:info:- | grep black; done | sort -n
