#!/bin/bash -xe

for id in $(grep "id=\"led" mesh.svg | sed -e 's/.*="//' -e 's/".*//'); do echo $id; inkscape --export-background=\#000000 --export-area-page -w 160 -h 120 --export-id=$id --export-id-only --export-png=$(pwd)/mesh-$id.png $(pwd)/mesh.svg; done
